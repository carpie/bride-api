# bride-api

A simple example backend service that serves up quotes from an old movie. This
service was created as a demonstration of deploying a backend service on a
Raspberry Pi Kubernetes cluster using k3s.
