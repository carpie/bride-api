const express = require('express');

const quotes = [
  'As you wish.\n',
  'Rodents of Unusual Size? I don\'t think they exist.\n',
  '"We\'ll never survive!" "Nonsense! You\'re only saying that because no one ever has."\n',
  'Hello. My name in Inigo Montoya. You killed my father. Prepare to die!\n',
  'Inconceivable!\n',
  'You keep using that word. I do not think it means what you think it means.\n',
  'Let me explain... No, there is too much. Let me sum up.\n',
  'I haven\'t fought one person for so long! I\'ve specialized in groups - battling gangs for local charities, that kind of thing.\n',
  'I always think that everything could be a trap, which is why I\'m still alive.\n',
  'You rush a miracle man, you get rotten miracles.\n',
  'I\'ve seen worse!\n',
  'There\'s a big difference between mostly deaad and all dead. Now, mostly dead is slightly alive.\n',
  'Alright, now let\'s see, where were we? Oh, yes. In the pit of despair.\n',
  'It\'s very strange. I have been in the revenge business so long, now that it\'s over, I don\'t know what to do with the rest of my life.\n',
  'Have fun storming the castle!\n',
  'If he were all dead, there\'s only one thing you can do. Go through his pockets and look for loose change.\n',
  'You\'re trying to kidnap what I\'ve rightfully stolen!\n',
  'Let me put it this way, have you ever heard of Plato, Aristotle, Socrates? Morons.\n',
  'You\'ve been mostly dead all day\n.'
];

const app = express();
const port = 1987;


app.get('/', (req, res) => {
  res.send(quotes[Math.floor(Math.random() * quotes.length)]);
});

app.listen(port, () => console.log(`PB quotes being served on port ${port}`));
